import threading
import logging
import adisp
import certifi
import urllib
import urllib3

__all__ = ('fetchURL', )

_urllib_pool = None

def _init_urllib_pool():
    global _urllib_pool

    # skip if already inited
    if _urllib_pool:
        return

    proxy = None
    #TODO: reenable after WoT py2->py3 transition
    #if not proxy:
    #    proxy = urllib.getproxies().get("https")
    if not proxy:
        proxy = urllib.getproxies().get("http")

    opts =  {
        "num_pools": 2,
        "cert_reqs": "CERT_REQUIRED",
        "ca_certs": certifi.where(),
    }

    if proxy:
        _urllib_pool = urllib3.ProxyManager(proxy, **opts)
    else:
        _urllib_pool = urllib3.PoolManager(**opts)

    logging.getLogger("urllib3").setLevel(logging.ERROR)

_init_urllib_pool()


def _threaded_request(url, callback, method='GET', headers={}, timeout=30.0, body=None):
    resp = _urllib_pool.request(method, url, headers=headers, body=body, timeout=timeout)
    callback((resp.status, resp.data))

@adisp.adisp_async
@adisp.adisp_process
def fetchURL(url, method='GET', headers={}, timeout=30.0, body=None, callback=None):
    status, data = yield lambda callback: threading.Thread(
        target=_threaded_request,
        args=(url, callback, method, headers, timeout, body)
    ).start()
    callback((status, data))
