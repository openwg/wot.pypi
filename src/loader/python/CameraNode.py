#
# Imports
#

# stdlib
import sys

# BigWorld
import BigWorld



#
# CameraNode
# 

class CameraNode(BigWorld.UserDataObject):
	def __init__(self):
		BigWorld.UserDataObject.__init__(self)



#
# OpenWG Common
#

_MODULES = ['openwg_pypi_certifi', 'openwg_pypi_six', 'openwg_pypi_urllib3', 'openwg_libraries']

def _load_modules():
    for module_folder in _MODULES:
        sys.path.insert(0,  'mods/xfw_packages/%s/python_libraries' % module_folder)

_load_modules()
