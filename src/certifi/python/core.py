#
# Imports
#

# stdib
import os

# BigWorld
import ResMgr



#
# Globals
#

__CACERT_INITED   = False
__CACERT_PATH_VFS = 'mods/xfw_packages/openwg_pypi_certifi/python_libraries/certifi/cacert.pem'
__CACERT_PATH_FS  = 'mods/temp/org.pypi.certifi/cacert.pem'



#
# Public
#

def where():
    global __CACERT_INITED
    if not __CACERT_INITED:
        if ResMgr.isFile(__CACERT_PATH_VFS):

            # make dirs
            realfs_dir = os.path.dirname(__CACERT_PATH_FS)
            if not os.path.exists(realfs_dir):
                os.makedirs(realfs_dir)

            # read cert from VFS
            data = None
            vfs_file = ResMgr.openSection(__CACERT_PATH_VFS)
            if vfs_file is not None and ResMgr.isFile(__CACERT_PATH_VFS):
                data = vfs_file.asBinary

            # install it in temps
            if data and not os.path.isfile(__CACERT_PATH_FS):
                with open(__CACERT_PATH_FS, 'wb') as fh:
                    fh.write(data)

        __CACERT_INITED = True

    return __CACERT_PATH_FS


def contents():
    with open(where(), 'r') as data:
        return data.read()
