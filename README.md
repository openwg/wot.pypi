# OpenWG.WoT.Common

A set of libraries to simplify the creation of WoT mods



## Contents

* pypi
  * certifi
  * six
  * urllib3

* libraries
  * openwg_network
  * openwg_vfs





## OpenWG.Network

A library to simplify interaction with web servers

### fetchURL()

Implementation of `BigWorld.fetchURL()` on top of the urllib3 library

```python
import adisp
import openwg_network

@adisp.adisp_process
def requestData():
  request_context = {
    'url': 'https://example.com',
    'method': 'GET',
    'headers': {},
    'timeout': 30.0
  }
  response_code, response_body = yield openwg_network.fetchURL(**request_context)
  print (response_code, response_body)
```



## OpenWG.VFS

A library to simplify interaction with BigWorld VFS (Virtual File System)
