# SPDX-License-Identifier: LGPL-3.0+
# Copyright (c) 2017-2024 OpenWG Contributors



Import-Module "$PSScriptRoot/src_build/library.psm1" -Force -DisableNameChecking
Build-Package -PackageDirectory "$PSScriptRoot/src" -OutputDirectory "$PSScriptRoot/~output"
