# Changelog

## v1.0.2

* openwg_vfs: add `recursive` parameter to `openwg_vfs.directory_list_files()` function

## v1.0.1

* openwg_vfs: new library

## v1.0.0

* first version
